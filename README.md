Brat reader
===========
[![Checked with mypy][mypy_badge]](http://mypy-lang.org/)
[![Code style: black][black_badge]](https://github.com/psf/black)
[![Imports: isort][isort_badge]](https://pycqa.github.io/isort/) \
[![GitLab license][MIT_license_badge]][license_link]

Contribute
----------
- [Source Code](https://gitlab.com/unrndm/brat_reader)
- [Merge Requests](https://gitlab.com/unrndm/brat_reader/-/merge_requests)

Support
-------
If you are having issues, please let us know through
[issue tracker](https://gitlab.com/unrndm/brat_reader/-/issues).

License
-------
The project is licensed under the MIT license.

<!-- links -->
[mypy_badge]: https://img.shields.io/badge/mypy-checked-2a6db2?style=flat-square
[black_badge]: https://img.shields.io/badge/code%20style-black-000000.svg?style=flat-square
[isort_badge]: https://img.shields.io/badge/imports-isort-1674b1?style=flat-square&labelColor=ef8336

[MIT_license_badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[license_link]: https://gitlab.com/unrndm/brat_reader/blob/master/LICENSE
