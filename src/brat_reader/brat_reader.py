"""
main file of brat reader
"""

from __future__ import annotations

import logging
from collections.abc import Callable
from math import inf
from pathlib import Path
from typing import Any, TypedDict, Literal
from typing_extensions import NotRequired

from .classes import (
    BratAttribute,
    BratBase,
    BratEntity,
    BratRelation,
    Span,
    SuperEntity,
)

from .tokenizers import razdel_tokenizer
from .types import (
    Tokenizer,
    FilterSuperEntity,
)

brat_classes: list[type[BratBase]] = [
    BratEntity,
    BratRelation,
    BratAttribute,
]

brat_name2class: dict[str, type[BratBase]] = {
    cls.BRAT_NAME: cls for cls in brat_classes
}

brat_char2brat_name: dict[str, str] = {
    cls.BRAT_CHAR: cls.BRAT_NAME for cls in brat_classes
}


class Annotations(TypedDict):
    entity: NotRequired[list[BratEntity] | None]
    relation: NotRequired[list[BratRelation] | None]
    attribute: NotRequired[list[BratAttribute] | None]


class BratReader:
    MULTIPLE_TAG: str = "B-MULTIPLE"

    def __init__(self, source: Path | str, max_depth=42):
        self.logger = logging.getLogger("BratReader")
        self.logger.info("initializing Brat reader")

        self.document2annotations: dict[tuple[str, ...], Annotations] = {}
        self.document2text: dict[tuple[str, ...], str] = {}

        self.source = Path(source).resolve()
        self.max_depth = int(max_depth)

        self.process_path(self.source, self.max_depth)

    def __repr__(self) -> str:
        return f"BratReader(source={self.source}, max_depth={self.max_depth})"

    # def __str__(self) -> str:
    #     pass

    def process_path(self, source: Path, max_depth: int):
        if source.is_dir():
            self.logger.debug("processing {source} as directory")
            self.process_dir(self.source, [], max_depth)
        elif source.is_file():
            self.logger.debug(f"processing {source} as file")
            self.process_file(self.source, [])
        else:
            self.logger.error(f"unknown source_path object {self.source}")
            raise ValueError(f"unknown source_path object {self.source}")

    def process_dir(self, folder: Path, prefix: list[str], depth: int):
        self.logger.debug(f"processing dir {folder}")

        if depth <= 0:
            return

        prefix.append(folder.name)

        for el in folder.iterdir():
            if el.is_file():
                self.process_file(el, prefix)
            elif el.is_dir():
                self.process_dir(el, prefix, depth - 1)

    def process_file(self, file: Path, prefix: list[str]):
        self.logger.debug(f"processing file `{file}`")

        name = tuple(list(prefix.copy()) + [file.stem])

        if file.suffix == ".ann":
            self.logger.debug(f"reading annotation file {file}")

            annotations: Annotations = {}

            with file.open("r") as lines:
                for line in lines:
                    ann_type_char = line[0]
                    if ann_type_char not in brat_char2brat_name:
                        self.logger.warning(
                            f"unknown annotation type character: `{ann_type_char}`"
                        )
                        continue

                    ann_name = brat_char2brat_name[ann_type_char]
                    ann = brat_name2class[ann_name].from_line(line)

                    if ann_name not in annotations:
                        annotations[ann_name] = [ann]
                    else:
                        annotations[ann_name].append(ann)
            self.document2annotations[name] = annotations
        elif file.suffix == ".txt":
            self.logger.debug(f"reading text file `{file}`")
            self.document2text[name] = file.read_text()
        else:
            self.logger.warning(
                f"unknown file extension `{file.suffix}` of file `{file}`"
            )

    def get_entity_type2num_entities(self):
        entity_type2num_entities: dict[str, int] = {}
        for anns in self.document2annotations.values():
            if BratEntity.BRAT_NAME in anns:
                entities = anns[BratEntity.BRAT_NAME]
                for entity in entities:
                    if entity.entity_type not in entity_type2num_entities:
                        entity_type2num_entities[entity.entity_type] = 1
                    else:
                        entity_type2num_entities[entity.entity_type] += 1

        return entity_type2num_entities

    def get_document2super_entities(self):
        self.logger.debug("constructing super entities dict")

        document2super_entities: dict[tuple[str, ...], list[SuperEntity]] = {}
        for doc, anns in self.document2annotations.items():
            self.logger.debug(f"processing document `{doc}`")

            document2super_entities[doc] = []

            if BratEntity.BRAT_NAME in anns:
                self.logger.debug("processing entities in document")

                for entity in anns[BratEntity.BRAT_NAME]:
                    document2super_entities[doc].append(
                        SuperEntity.from_unfiltered_entity_entities_relations_atributes(
                            entity,
                            anns[BratEntity.BRAT_NAME],
                            anns.get(BratRelation.BRAT_NAME, []),
                            anns.get(BratAttribute.BRAT_NAME, []),
                        )
                    )
            else:
                self.logger.debug(
                    "document doesn't have entities in annotations, skipping"
                )
        return document2super_entities

    class BratReaderSubDict(TypedDict):
        text: str
        tokens: list[list[str]]
        labels: list[list[list[str] | Literal["O"]]]

    def to_dict(
        self,
        tokenier: Tokenizer = razdel_tokenizer,
        filter_se: FilterSuperEntity = lambda _: True,
    ):
        res: dict[tuple[str, ...], BratReader.BratReaderSubDict] = {}

        document2super_entities = self.get_document2super_entities()
        for doc, ses in document2super_entities.items():
            text = self.document2text[doc]

            tokenized_text = tokenier(text)

            tokens = []
            labels = []

            for sentence in tokenized_text:
                tokens.append([])
                labels.append([])
                for token in sentence:
                    tokens[-1].append(token["word"])

                    ses_filtered_by_spans = list(
                        filter(
                            lambda se: Span(token["start"], token["end"]) in se.span,
                            ses,
                        )
                    )
                    ses_filtered = list(
                        filter(
                            filter_se,
                            ses_filtered_by_spans,
                        )
                    )

                    if len(ses_filtered) == 0:
                        labels[-1].append("O")
                    else:
                        sentence_labels = []
                        for se in ses_filtered:
                            if se.span.start == token["start"]:
                                sentence_labels.append(f"B-{se.name}")
                            else:
                                sentence_labels.append(f"I-{se.name}")
                        labels[-1].append(sentence_labels)

            res[doc] = {
                "text": text,
                "tokens": tokens,
                "labels": labels,
            }

        return res
