from typing import Callable, TypedDict

from .classes import BratEntity, SuperEntity


class TokenizedWord(TypedDict):
    start: int
    end: int
    word: str


Tokenizer = Callable[[str], list[list[TokenizedWord]]]

FilterSuperEntity = Callable[[SuperEntity], bool]
