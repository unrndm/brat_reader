from razdel import sentenize, tokenize

from .types import TokenizedWord, Tokenizer


def razdel_tokenizer(text: str) -> list[list[TokenizedWord]]:
    return [
        [
            TokenizedWord(
                start=sentence.start + word.start,
                end=sentence.start + word.stop,
                word=word.text,
            )
            for word in tokenize(sentence.text)
        ]
        for sentence in sentenize(text)
    ]
