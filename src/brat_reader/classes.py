"""
main classes for brat_reader. Also contains NamedTuple definitions
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Literal, NamedTuple
from typing_extensions import Self


class Argument(NamedTuple):
    argument_id: str
    entity_id: int


class Span:
    """
    utility class for storing spans
    """

    def __init__(self, start: int, finish: int):
        self.start = int(start)
        self.finish = int(finish)

    @classmethod
    def from_string(cls, string: str, sep: str = ","):
        start, finish, *others = string.split(sep)
        start, finish = int(start), int(finish)
        return cls(start, finish)

    @classmethod
    def from_spans(cls, spans: list[Span]):
        mins, maxs = [], []
        for span in spans:
            if span.start < span.finish:
                mins.append(span.start)
                maxs.append(span.finish)
            else:
                maxs.append(span.start)
                mins.append(span.finish)
        return Span(min(mins), max(maxs))

    def to_dict(self) -> dict[str, Any]:
        return {
            "start": self.start,
            "finish": self.finish,
        }

    def as_tuple(self) -> tuple[int, int]:
        return (self.start, self.finish)

    def __getitem__(self, item: Literal[0, 1]) -> int:
        return self.as_tuple()[item]

    def left_overlap(self, other: Span) -> bool:
        """checks if `self` overlaps on the left with `other` span

        Parameters
        ----------
        other : Span
            other span to check left overlap

        Returns
        -------
        bool
            self overlaps other on left
        """
        return self.start <= other.start <= self.finish

    def other_in_self(self, other: Span) -> bool:
        """checks if other span is inside of self

        Parameters
        ----------
        other : Span
            other span to check if other is in span

        Returns
        -------
        bool
            True if other is inside of self, False otherwise
        """
        return self.start <= other.start and other.finish <= self.finish

    def __contains__(self, item: Span) -> bool:
        """checks if other span is inside of self

        Parameters
        ----------
        item : Span

        Returns
        -------
        bool
            True if other is inside of self, False otherwise
        """
        return self.other_in_self(item)

    def overlap(self, other: Span):
        return (
            self.left_overlap(other)
            or self.other_in_self(other)
            or other.left_overlap(self)
            or other.other_in_self(self)
        )

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Span):
            return self.start == other.start and self.finish == other.finish
        else:
            raise NotImplementedError

    def __hash__(self) -> int:
        return hash(self.as_tuple())

    def __str__(self) -> str:
        return f"{self.start} {self.finish}"

    def __repr__(self) -> str:
        return f"Span(start={self.start}, finish={self.finish})"


"""
next classes follow brat annotaiton as described [here](`http://brat.nlplab.org/standoff.html`)

Note:
-----
# and * are ignored
citation from docs:
    ```
    additionally, # and * are used, but they are too special cases
    and should be dealt in special ways
    ```
"""


class BratBase(ABC):
    """
    base class for all brat annotation types
    """

    BRAT_CHAR: str = ""
    BRAT_NAME: str = ""

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        if instance.BRAT_CHAR is None:
            raise Exception("Every instance of brat annotation must have unique CHAR")
        if instance.BRAT_NAME is None:
            raise Exception("Every instance of brat annotation must have unique NAME")
        return instance

    @classmethod
    @abstractmethod
    def from_line(cls, line: str) -> Self:
        """
        main function for creating annotation type

        Parameters
        ----------
        line : str
            line from brat annotation file

        Returns
        -------
        Self
            new instance of class
        """
        raise NotImplementedError("method `from_line` must be implemented")

    @abstractmethod
    def __str__(self) -> str:
        raise NotImplementedError("method `__str__` must be implemented")

    @abstractmethod
    def __repr__(self) -> str:
        raise NotImplementedError("method `__repr__` must be implemented")

    @abstractmethod
    def _repr_pretty_(self, printer: Any, cycle: bool):
        start, end = "BratBase(", ")"
        printer.text(f"{start}...{end}")
        raise NotImplementedError("method `_repr_pretty_` must be implemented")

    @abstractmethod
    def to_dict(self) -> dict:
        raise NotImplementedError("method `to_dict` must be implemented")


class BratEntity(BratBase):
    BRAT_CHAR: str = "T"
    BRAT_NAME: str = "entity"

    def __init__(
        self,
        entity_id: int,
        entity_type: str,
        spans: list[Span],
        form: str,
    ):
        self.entity_id = int(entity_id)
        self.entity_type = str(entity_type)
        self.spans = spans
        self.form = str(form)

    @classmethod
    def from_line(cls, line: str):
        entity_char_id, meta, *form = line.strip().split("\t")

        entity_char, entity_id = entity_char_id[0], int(entity_char_id[1:])
        assert entity_char == cls.BRAT_CHAR

        meta_split = meta.split(" ")
        entity_type: str = meta_split[0]
        spans_str = " ".join(meta_split[1:])

        form = "\t".join(form).strip()

        spans = [Span.from_string(sp, " ") for sp in spans_str.split(";")]

        return cls(entity_id, entity_type, spans, form)

    def __str__(self):
        return "{brat_char}{id}\t{type} {spans}\t{form}".format(
            brat_char=self.BRAT_CHAR,
            id=self.entity_id,
            type=self.entity_type,
            spans=";".join([str(span) for span in self.spans])[:-1],
            form=self.form,
        )

    def __repr__(self):
        return (
            "BratEntity("
            f"entity_id={self.entity_id}, "
            f"entity_type='{self.entity_type}', "
            f"spans={self.spans}, "
            f"form='{self.form}'"
            ")"
        )

    def _repr_pretty_(self, printer: Any, cycle: bool):
        start, end = "BratEntity(", ")"
        if cycle:
            printer.text(f"{start}...{end}")
        else:
            with printer.group(len(start), start, end):
                printer.breakable()
                printer.text("entity_id=")
                printer.pretty(self.entity_id)
                printer.text(",")
                printer.breakable()
                printer.text("entity_type=")
                printer.pretty(self.entity_type)
                printer.text(",")
                printer.breakable()
                printer.text("spans=")
                printer.pretty(self.spans)
                printer.text(",")
                printer.breakable()
                printer.text("form=")
                printer.pretty(self.form)
                printer.text(",")
                printer.breakable()

    def to_dict(self):
        return {
            "BRAT_NAME": self.BRAT_NAME,
            "BRAT_CHAR": self.BRAT_CHAR,
            "entity_id": self.entity_id,
            "entity_type": self.entity_type,
            "spans": [s.to_dict() for s in self.spans],
            "form": self.form,
        }


class BratRelation(BratBase):
    BRAT_CHAR: str = "R"
    BRAT_NAME: str = "relation"

    def __init__(self, relation_id: int, relation_type: str, arguments: list[Argument]):
        self.relation_id = int(relation_id)
        self.relation_type = str(relation_type)
        self.arguments = arguments

    @classmethod
    def from_line(cls, line: str) -> BratRelation:
        relation_char_id, meta = line.strip().split("\t")

        relation_char, relation_id = relation_char_id[0], int(relation_char_id[1:])
        assert relation_char == cls.BRAT_CHAR

        meta_split = meta.split(" ")

        relation_type = meta_split[0]
        arguments_str = " ".join(meta_split[1:])
        arguments = [
            Argument(sp.split(":")[0], int(sp.split(":")[1][1:]))
            for sp in arguments_str.split(" ")
        ]
        return cls(relation_id, relation_type, arguments)

    def __str__(self):
        return "{brat_char}{id}\t{type} {arguments}".format(
            brat_char=self.BRAT_CHAR,
            id=self.relation_id,
            type=self.relation_type,
            arguments="".join(
                [
                    f"{argument.argument_id}:{argument.entity_id} "
                    for argument in self.arguments
                ]
            )[:-1],
        )

    def __repr__(self):
        return (
            "BratRelation("
            f"relation_id={self.relation_id}, "
            f"relation_type='{self.relation_type}', "
            f"arguments={self.arguments})"
        )

    def _repr_pretty_(self, printer: Any, cycle: bool):
        start, end = "BratRelation(", ")"
        if cycle:
            printer.text(f"{start}...{end}")
        else:
            with printer.group(len(start), start, end):
                printer.breakable()
                printer.text("relation_id=")
                printer.pretty(self.relation_id)
                printer.text(",")
                printer.breakable()
                printer.text("relation_type=")
                printer.pretty(self.relation_type)
                printer.text(",")
                printer.breakable()
                printer.text("arguments=")
                printer.pretty(self.arguments)
                printer.text(",")
                printer.breakable()

    def to_dict(self):
        return {
            "BRAT_CHAR": self.BRAT_CHAR,
            "BRAT_NAME": self.BRAT_NAME,
            "relation_id": self.relation_id,
            "relation_type": self.relation_type,
            "arguments": [a._asdict() for a in self.arguments],
        }


class BratAttribute(BratBase):
    BRAT_CHAR: str = "A"
    BRAT_NAME: str = "attribute"

    def __init__(
        self,
        attribute_id: int,
        attribute_name: str,
        entity_id: int,
        value: str | None = None,
    ):
        self.attribute_id = int(attribute_id)
        self.attribute_name = str(attribute_name)
        self.entity_id = int(entity_id)
        self.value = str(value) if value is not None else None

    @classmethod
    def from_line(cls, line: str):
        attribute_char_id, meta = line.strip().split("\t")

        attribute_char, attribute_id = attribute_char_id[0], int(attribute_char_id[1:])
        assert attribute_char == cls.BRAT_CHAR

        meta_split = meta.split(" ")
        attribute_name = meta_split[0]
        entity_id = int(meta_split[1].strip()[1:])

        value = None
        if len(meta_split) > 2:
            value = meta_split[2].strip()

        return cls(attribute_id, attribute_name, entity_id, value)

    def __str__(self):
        return "{brat_char}{id}\t{attribute_name} {entity_id} {value}".format(
            brat_char=self.BRAT_CHAR,
            id=self.attribute_id,
            attribute_name=self.attribute_name,
            entity_id=self.entity_id,
            value="" if self.value is None else self.value,
        ).strip()

    def __repr__(self):
        return (
            "BratAttribute("
            f"attribute_id={self.attribute_id}, "
            f"attribute_name='{self.attribute_name}', "
            f"entity_id='{self.entity_id}', "
            f"value='{self.value}')"
        )

    def _repr_pretty_(self, printer: Any, cycle: bool):
        start = "BratAttribute("
        end = ")"
        if cycle:
            printer.text(f"{start}...{end}")
        else:
            with printer.group(len(start), start, end):
                printer.breakable()
                printer.text("attribute_id=")
                printer.pretty(self.attribute_id)
                printer.text(",")
                printer.breakable()
                printer.text("attribute_name=")
                printer.pretty(self.attribute_name)
                printer.text(",")
                printer.breakable()
                printer.text("entity_id=")
                printer.pretty(self.entity_id)
                printer.text(",")
                printer.breakable()
                printer.text("value=")
                printer.pretty(self.value)
                printer.text(",")
                printer.breakable()

    def to_dict(self):
        return {
            "BRAT_CHAR": self.BRAT_CHAR,
            "BRAT_NAME": self.BRAT_NAME,
            "attribute_id": self.attribute_id,
            "attribute_name": self.attribute_name,
            "entity_id": self.entity_id,
            "value": self.value,
        }


class SuperEntity(BratEntity):
    def __init__(
        self,
        entity: BratEntity,
        relations: list[BratRelation],
        rel_type2entities: dict[str, list[BratEntity]],
        attributes: list[BratAttribute],
        attr_name2attr_value: dict[str, Any | None],
    ):
        self.entity = entity

        self.relations = relations
        self.rel_type2entities = rel_type2entities

        self.attributes = attributes
        self.attr_name2attr_value = attr_name2attr_value

    @property
    def span(self):
        return Span.from_spans(
            [
                *self.entity.spans,
                *[
                    Span.from_spans(ent.spans)
                    for ents in self.rel_type2entities.values()
                    for ent in ents
                ],
            ]
        )

    @property
    def name(self):
        if len(self.attributes) > 0:
            return f"{self.entity.entity_type} [{' '.join([f'{name}:{value}' for name, value in self.attr_name2attr_value.items()])}]"
        else:
            return self.entity.entity_type

    @classmethod
    def from_entity_entities_relations_atributes(
        cls,
        entity: BratEntity,
        entities: list[BratEntity],
        relations: list[BratRelation],
        attributes: list[BratAttribute],
    ):
        entity_id2entity = {ent.entity_id: ent for ent in entities}
        rel_type2entities = {
            r.relation_type: [entity_id2entity[arg.entity_id] for arg in r.arguments]
            for r in relations
        }
        attr_name2attr_value = {attr.attribute_name: attr.value for attr in attributes}
        return cls(
            entity, relations, rel_type2entities, attributes, attr_name2attr_value
        )

    @classmethod
    def from_unfiltered_entity_entities_relations_atributes(
        cls,
        entity: BratEntity,
        entities: list[BratEntity],
        relations: list[BratRelation],
        attributes: list[BratAttribute],
    ):
        # filter relations to only include those that have any relation to entity
        relations = list(
            filter(
                lambda rel: any(
                    [arg.entity_id == entity.entity_id for arg in rel.arguments]
                ),
                relations,
            )
        )
        attributes = list(
            filter(
                lambda attr: attr.entity_id == entity.entity_id,
                attributes,
            )
        )

        return cls.from_entity_entities_relations_atributes(
            entity,
            entities,
            relations,
            attributes,
        )

    def __str__(self):
        return "\n".join(
            [
                str(self.entity),
                *[str(rel) for rel in self.relations],
                *[str(attr) for attr in self.attributes],
            ]
        )

    def __repr__(self):
        return (
            "SuperEntity("
            f"entity={self.entity!r}, "
            f"relations='{self.relations!r}', "
            f"attributes='{self.attributes!r}'"
            ")"
        )

    def _repr_pretty_(self, printer: Any, cycle: bool):
        start, end = "SuperEntity(", ")"
        if cycle:
            printer.text(f"{start}...{end}")
        else:
            with printer.group(len(start), start, end):
                printer.breakable()
                printer.text("entity=")
                printer.pretty(self.entity)
                printer.text(",")
                printer.breakable()
                printer.text("relations=")
                printer.pretty(self.relations)
                printer.text(",")
                printer.breakable()
                printer.text("attributes=")
                printer.pretty(self.attributes)
                printer.text(",")
                printer.breakable()

    def to_dict(self):
        return {
            "BRAT_CHAR": self.BRAT_CHAR,
            "BRAT_NAME": self.BRAT_NAME,
            "entity": self.entity.to_dict(),
            "relations": [r.to_dict() for r in self.relations],
            "rel_type2entities": self.rel_type2entities,
            "attributes": [a.to_dict() for a in self.attributes],
            "attr_name2attr_value": self.attr_name2attr_value,
        }
